from .models import Customer
from .serializers import CustomerCreateSerializer
from rest_framework import generics
# Create your views here.

class CustomerCreateAPIView(generics.CreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerCreateSerializer