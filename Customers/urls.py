from .views import CustomerCreateAPIView
from django.urls import path

urlpatterns = [
    path('create', CustomerCreateAPIView.as_view()),
]
