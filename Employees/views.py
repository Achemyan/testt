from django.shortcuts import render

# Create your views here.

from .models import Employee
from .serializers import EmployeeCreateSerializer
from rest_framework import generics
# Create your views here.

class EmployeeCreateAPIView(generics.CreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeCreateSerializer