from .views import EmployeeCreateAPIView
from django.urls import path

urlpatterns = [
    path('create', EmployeeCreateAPIView.as_view()),
]
