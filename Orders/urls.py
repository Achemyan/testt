from .views import OrderCreateAPIView, OrderListAPIView
from django.urls import path

urlpatterns = [
    path('create', OrderCreateAPIView.as_view()),
    path('all', OrderListAPIView.as_view()),
]