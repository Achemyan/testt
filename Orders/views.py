from django.shortcuts import render

# Create your views here.

from .models import Order
from .serializers import OrderCreateSerializer, OrderListSerializer
from rest_framework import generics


# Create your views here.

class OrderCreateAPIView(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer

class OrderListAPIView(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer
