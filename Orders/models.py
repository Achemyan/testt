from django.db import models

class Order(models.Model):
    CustomerID = models.CharField(max_length=30)
    EmployeeID = models.CharField(max_length=30)
    ShipperID = models.CharField(max_length=30)
    Address = models.CharField(max_length=30)
