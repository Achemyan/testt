from django.shortcuts import render

# Create your views here.

from .models import OrderDetail
from .serializers import OrderDetailCreateSerializer
from rest_framework import generics


# Create your views here.

class OrderDetailCreateAPIView(generics.CreateAPIView):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailCreateSerializer