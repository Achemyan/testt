from .views import OrderDetailCreateAPIView
from django.urls import path

urlpatterns = [
    path('create', OrderDetailCreateAPIView.as_view()),
]
