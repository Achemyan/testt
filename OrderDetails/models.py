from django.db import models

# Create your models here.

class OrderDetail(models.Model):
    OrderID = models.IntegerField()
    ProductID = models.IntegerField()
    Quantity = models.IntegerField()
    DelTime = models.CharField(max_length=30)
